#include <iostream>
#include <vector>
#include <string>

using namespace std;
template<typename T>
class Stack
{
public:
    void push(T);
    T pop();
    void show();
private:
    vector<T> v;
};

int main() {
    Stack<int> a;
    a.push(1);  a.push(2);  a.push(3); a.push(4); a.push(5); a.push(6); a.push(7); a.push(8); a.push(9); a.push(10);
    a.show();
    cout << "Pop: " << a.pop() << endl;
    a.show();

    Stack<float> b;
    b.push(1.20); b.push(1.40); b.push(1.50);
    b.show();
    cout << "Pop: " << b.pop() << endl;
    b.show();

    Stack<double> c;
    c.push(4.34); c.push(5.004); c.push(6.574);
    c.show();
    cout << "Pop: " << c.pop() << endl;
    c.show();

    Stack<string> d;
    d.push("One"); d.push("Two"); d.push("Three");
    d.show();
    cout << "Pop: " << d.pop() << endl;
    d.show();

    return 0;
}

template<class T> void Stack<T>::push(T elem)
{
    v.push_back(elem);
}

template<class T> T Stack<T>::pop()
{
    T elem = v.back();
    v.pop_back();
    return elem;
}
template<class T> void Stack<T>::show()
{
    cout << "Stack: ";
    for (auto e : v) cout << e << " ";
    cout << endl;
}